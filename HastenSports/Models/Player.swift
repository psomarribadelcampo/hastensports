//
//  Player.swift
//  HastenSports
//
//  Created by Pablo Somarriba del Campo on 27/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import Foundation
import UIKit

struct Player: Decodable {
    var type: String
    var title: String
    var players: [Players] = []
}

struct Players: Decodable {
    var id: Int!
    var image: String!
    var surname: String!
    var name: String!
}

struct ImagePlayer {
    
    var id: Int = 0
    var imageView: UIImageView = UIImageView()
    
}

struct PlayersResponse: Decodable {
    var players: [Player]
}

extension Player {
    init?(json: JSON) {
        guard let type = json["type"] as? String else {
            return nil
        }
        guard let title = json["title"] as? String else {
            return nil
        }
        
        if let data = json["players"] as? [[String: String]] {
            for reg in data {
                var miPlayers = Players()
                if let image = reg["image"] {
                    miPlayers.image = image
                }
                if let name = reg["name"] {
                    miPlayers.name = name
                }
                if let surname = reg["surname"] {
                    miPlayers.surname = surname
                }
                self.players.append(miPlayers)
            }
        }
        
        self.type = type
        self.title = title
    }
}
