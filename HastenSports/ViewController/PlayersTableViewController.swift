//
//  PlayersTableViewController.swift
//  HastenSports
//
//  Created by Pablo Somarriba del Campo on 28/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import UIKit

class PlayersTableViewController: UITableViewController {

    var listPlayersTable: [Player] = []
    var listImagesPlayerTable: [ImagePlayer] = []
    
    @IBOutlet var tableViewPlayers: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.listPlayersTable.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.listPlayersTable[section].players.count
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.listPlayersTable[section].title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPlayer", for: indexPath) as! PlayersTableViewCell
        var campos: Players
        
        campos = self.listPlayersTable[indexPath.section].players[indexPath.row]
        cell.name.text = campos.name
        cell.surname.text = campos.surname
        //cell.imagePlayer.contentMode = .scaleAspectFit
        
        //round image
        cell.imagePlayer.layer.cornerRadius = cell.imagePlayer.frame.size.width / 2
        cell.imagePlayer.clipsToBounds = true

        //Search image player for id
        let idAux = campos.id
        for reg in listImagesPlayerTable where reg.id == idAux {
            cell.imagePlayer.image = reg.imageView.image
        }

        return cell
    }
    // MARK: - Table view data source


}
