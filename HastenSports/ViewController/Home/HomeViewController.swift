//
//  HomeViewController.swift
//  HastenSports
//
//  Created by Pablo Somarriba del Campo on 27/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    var listPlayers: [Player] = []
    var listImagePlayers: [ImagePlayer] = []
    var contId: Int = 0
    
    @IBOutlet weak var loadPlayer: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var go: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = home
        
        self.spinner.isHidden = true
        self.info.isHidden = true
        self.go.isHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.spinner.isHidden = true
        self.info.isHidden = true
        self.go.isHidden = true
        self.loadPlayer.isHidden = false

    }
    
    func loadImagesPlayers() {
        var contSect: Int = 0
        var contPlay: Int = 0
        for regSection in listPlayers {
            for regPlayers in regSection.players {
                //let cadAux = regPlayers.image.replacingOccurrences(of: "http", with: "https")
                let cadAux = regPlayers.image
                if let checkedUrl = URL(string: cadAux!) {
                    var myImage = ImagePlayer()
                    myImage.id = self.contId
                    contId+=1
                    PlayerImage().downloadImage(url: checkedUrl, imgView: myImage.imageView)
                    self.listImagePlayers.append(myImage)
                    self.listPlayers[contSect].players[contPlay].id = myImage.id
                }
                contPlay += 1
            }
            contSect += 1
            contPlay = 0
        }

        self.spinner.stopAnimating()
        self.spinner.isHidden = true
        self.info.isHidden = false
        self.go.isHidden = false
        self.info.text = ok

        

        
    }
    
    func loadPlayers() {
        var playersTask: URLSessionDataTask!

        playersTask?.cancel() //Cancel previous loading task.

        self.loadPlayer.isHidden = true
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        
        playersTask = PlayerService().loadPlayers(forUser: self.listPlayers) {[weak self] dataPlayers, error in
            DispatchQueue.main.async {
                
                if let error = error {
                    print(error.localizedDescription) //Handle service error
                    self?.info.text = error.localizedDescription
                } else if let dataPlayersAux = dataPlayers {
                    print("Players OK")
                    self?.listPlayers = dataPlayersAux
                    self?.loadImagesPlayers()
                }
                
            }
        }

    }
    @IBAction func loadPlayers(_ sender: UIButton) {
        
        self.loadPlayers()

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: home, style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        if segue.identifier == "showplayers" {
            let upcoming: PlayersTableViewController = segue.destination as! PlayersTableViewController
            upcoming.listPlayersTable = self.listPlayers
            upcoming.listImagesPlayerTable = self.listImagePlayers
        }
    }
    
}
