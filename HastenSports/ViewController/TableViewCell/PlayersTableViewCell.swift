//
//  PlayersTableViewCell.swift
//  HastenSports
//
//  Created by Pablo Somarriba del Campo on 28/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import UIKit

class PlayersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imagePlayer: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var surname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
